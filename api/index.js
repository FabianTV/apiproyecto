'use strict'

const express = require('express')
const bodyParser = require('body-parser');
const requestjson = require('request-json');
const app = express()
const port = process.env.PORT || 3000
const path = require('path')
const mongoose = require('mongoose')
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var calls = require("./calls.js")
const nodemailer = require('nodemailer');
var rand = require('generate-key');


app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/mtorres/collections",
    apikey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLab = requestjson.createClient(urlMlabRaiz);
var clienteMLabRaiz;

app.listen(port, () => {
  console.log(`API REST en localhost:${port}`)
})


app.get('/pruebas', (req, res) =>{
})



app.post('/login', (req, res) => {
  var email = req.headers.email;
  var password = req.headers.password;
  var query = 'q={email:"'+ email +'", password:"'+ password +'"}';
  clienteMLabRaiz = requestjson.createClient(urlMlabRaiz+"/Clientes?"+apikey+"&"+query);
  clienteMLabRaiz.get ('', function(err, reaM, body) {

    if(!err){
      if(body.length == 1){
        var request = new XMLHttpRequest();
        query = 'q={email:"'+email+'"}'
        request.open("GET", "https://api.mlab.com/api/1/databases/mtorres/collections/Movs?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&"+query, false);
        request.responseType = 'json';
        request.onload = function(e) {
          if (this.status == 200) {
            var data = JSON.parse(request.responseText);
            var firstPost = data[0];
            var objR = {"nombre":body[0].name,"data":[]};
            data.forEach(function (post) {
               objR.data.push({"motivo":post.leyenda, "cantidad":post.amount})
             });
            res.status(200).send(objR);
          }else{
            res.status(300).send(objR)
          }
        };
        request.send();
      }else{
        res.status(400).send('sin registro');
      }
    } else{
      res.send(body);
    }
  })
})


app.post('/insertar/cliente', (req, res) => {

  var data = {
    email: req.headers.email,
    password: req.headers.password,
    name: req.headers.nombre,
    card: req.headers.card
  }

  var respuesta = calls.getCiente(data.email);
  var respJson = JSON.parse(respuesta);
  var longitud = respJson.length;

  switch (longitud) {
    case 0:
      var url = "https://api.mlab.com/api/1/databases/mtorres/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
      clienteMLabRaiz = requestjson.createClient(url);
      clienteMLabRaiz.post ('', data, function(err, reaM, body) {
        if(!err){
          res.status(200).send("exit0");
        } else{
          res.status(303).send("error de insercion");
        }
      })
      break;

    case 1:
      res.status(203).send("ya existe");
      break;

    default:
      res.status(404).send("ERROR|");
      break;
  }
})


app.post('/insertar/mov', (req, res) => {
  var data = {
    email: req.headers.email,
    leyenda: req.headers.leyenda,
    amount: req.headers.amount
  }
  var url = "https://api.mlab.com/api/1/databases/mtorres/collections/Movs?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var client = requestjson.createClient(url);
  client.post('', data, function(err, reaM, body){
    if(!err){
      res.status(200).send("procesado");
      return 200;
    }else{
      res.status(400).send(err);
      return null;
    }
  })
})


app.post('/send', (req, res)=>{
  var pass = rand.generateKey(20);
  var mensaje = "Hola su clave es " + pass;
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: req.headers.email,
    from: 'security@bank.com',
    subject: 'Bank',
    text: 'Correo para restituir contraseña',
    html: '<h3>'+ mensaje +'</h3>',
  };
  sgMail.send(msg);

  var data = {
    email : req.headers.email,
    temporal:pass
  }

  var url = "https://api.mlab.com/api/1/databases/mtorres/collections/Temp?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var client = requestjson.createClient(url);
  client.post('', data, function(err, reaM, body){
    if(!err){
      res.status(200).send("ok");
      console.log(mensaje);
      return 200;
    }else{
      console.log("error insertar temp en la base");
      console.log(reaM);
      res.status(400).send("no");
      return null;
    }
  })
})


app.post('/cambiar', (req, res)=>{
  var data = {
    email: req.headers.email,
    temporal: req.headers.pass
  }
  var respuesta = calls.confirm(data);
  console.log("validacion  ");
  console.log(respuesta);
  switch(respuesta.numero){
    case 0:
      var continuar = calls.delete(respuesta.id);
      if(continuar == 1){
        var query = 'q={"email":"'+data.email+'"}';
        var url = "https://api.mlab.com/api/1/databases/mtorres/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&"+query;
        var client = requestjson.createClient(url);

        client.put('', {"$set":{"password":req.headers.cont}}, function(err, reaM, body){
          if(!err){
            res.status(200).send("ok");
            return 200;
          }else{
            res.status(400).send("no");
            return null;
          }
        })
      }else if(continuar == 0){
        res.status(201).send("error al borrar")
      }
      break;

    case 1:
      res.status(202).send("password incorrecta");
      break;

    case 2:
      res.status(203).send("error, email sin proceso");
      break;

    case 3:
      res.status(204).send("multiples intentos de recuperar, borrando");
      var request = new XMLHttpRequest();
      respuesta.lst.forEach(function (keys) {
         request.open("DELETE", "https://api.mlab.com/api/1/databases/mtorres/collections/Temp/"+keys.val+"?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt", false);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         request.send();
       });
      break;

    case -1:
      res.status(401).send(":(")
      break;

    default:
      res.status(303).send("XD");
      break;

  }
})
