const requestjson = require('request-json');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const urlMlabRaiz = "https://api.mlab.com/api/1/databases/mtorres/collections"
const clienteMLab = requestjson.createClient(urlMlabRaiz);

exports.saludaDos = function(nombre){
  return "Hola " + nombre;
};


exports.getCiente = function(correo){
  var request = new XMLHttpRequest();
  var query = 'q={"email":"'+correo+'"}'

  request.open("GET", "https://api.mlab.com/api/1/databases/mtorres/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&"+query, false);
  request.setRequestHeader("Accept", "application/json");
  request.setRequestHeader("Content-Type", "application/json");
  console.log("enviando peticion "+ query);
  request.send();
  if(request.status == 200){
    console.log("peticion respondida");
    var respuesta = request.responseText
    console.log(respuesta);
    return respuesta;
  }else{
    console.log("peticion rechasada");
    return -1;
  }

};

exports.confirm = function(data){
  var request = new XMLHttpRequest();
  var query = 'q={"email":"'+data.email+'"}'
  request.open("GET", "https://api.mlab.com/api/1/databases/mtorres/collections/Temp?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&"+query, false);
  request.setRequestHeader("Accept", "application/json");
  request.setRequestHeader("Content-Type", "application/json");
  request.send();
  if(request.status == 200){
    var respuesta = JSON.parse(request.responseText);
    if(respuesta.length == 1){
      if(respuesta[0].temporal == data.temporal){
        var responder = {
          numero: 0,
          "id": respuesta[0]._id.$oid
        }
        return responder;
      }else{
        var responder = {
          numero: 1
        }
        return responder;
      }
    }else if(respuesta.length == 0){
      var responder = {
        numero: 2
      }
      return responder;
    }else{
      var responder = {
        numero: 3,
        "lst": []
      }
      respuesta.forEach(function(lst){
        responder.lst.push({"val":lst._id.$oid})
      });
      return responder;
    }
  }else{
    var responder = {
      numero: -1
    }
    return responder;
  }
};

exports.delete = function(id){
  var request = new XMLHttpRequest();
  request.open("DELETE", "https://api.mlab.com/api/1/databases/mtorres/collections/Temp/"+id+"?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt", false);
  request.setRequestHeader("Accept", "application/json");
  request.setRequestHeader("Content-Type", "application/json");
  request.send();
  if(request.status == 200){
    return 1;
  }else{
    return 0;
  }
};
